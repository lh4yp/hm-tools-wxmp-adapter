package top.hmtools.wxmp.account.apis;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import top.hmtools.wxmp.account.BaseTest;
import top.hmtools.wxmp.account.enums.EQRActionName;
import top.hmtools.wxmp.account.models.ActionInfo;
import top.hmtools.wxmp.account.models.QRCodeParam;
import top.hmtools.wxmp.account.models.QRCodeResult;
import top.hmtools.wxmp.account.models.Scene;
import top.hmtools.wxmp.account.models.TicketParam;

public class IQrcodeApisTest extends BaseTest {
	
	private IQrcodeApis qrcodeApis ;
	

	@Test
	public void testCreate() {
		QRCodeParam codeParam = new QRCodeParam();
		
		ActionInfo action_info = new ActionInfo();
		Scene scene = new Scene();
		scene.setScene_str("aaaabbbbcccc");
		action_info.setScene(scene);
		codeParam.setAction_info(action_info);
		
		codeParam.setAction_name(EQRActionName.QR_STR_SCENE);
		
		codeParam.setExpire_seconds(20*60*60*60L);
		QRCodeResult create = this.qrcodeApis.create(codeParam);
		this.printFormatedJson("创建二维码ticket", create);
	}

	@Test
	public void testShowQrCode() throws IOException {
		TicketParam ticketParam = new TicketParam();
		ticketParam.setTicket("gQF78DwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAyaV9LNVo3VUM5UV8xVi1qWWh1YzIAAgR_BtVeAwQAjScA");
		InputStream showQrCode = this.qrcodeApis.showQrCode(ticketParam);
		this.printFormatedJson("通过ticket换取二维码", IOUtils.toByteArray(showQrCode));
	}

	@Override
	public void initSub() {
		this.qrcodeApis = this.wxmpSession.getMapper(IQrcodeApis.class);
	}

}
