package top.hmtools.wxmp.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

import top.hmtools.wxmp.core.enums.HttpParamDataType;

@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WxmpParam {

	/**
	 * 参数名称
	 * @return
	 */
	@AliasFor("value")
	String name() default "";
	
	/**
	 * 参数名称
	 * @return
	 */
	String value() default "";
	
	/**
	 * 参数数据类型
	 * @return
	 */
	HttpParamDataType httpParamDataType() default HttpParamDataType.TEXT;
}
