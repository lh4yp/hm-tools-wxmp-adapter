package top.hmtools.wxmp.core;

import top.hmtools.wxmp.core.configuration.WxmpConfiguration;

/**
 * 会话工厂
 * @author HyboWork
 *
 */
public abstract class WxmpSessionFactory {

	/**
	 * 全局配置
	 */
	private WxmpConfiguration configuration;
	
	public WxmpSessionFactory( WxmpConfiguration configuration){
		this.configuration = configuration;
	}

	public WxmpConfiguration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(WxmpConfiguration configuration) {
		this.configuration = configuration;
	}
	
	/**
	 * 获取会话
	 * @return
	 */
	public abstract WxmpSession openSession();
}
