package top.hmtools.wxmp.core.model;

import java.lang.reflect.Method;

import top.hmtools.wxmp.core.model.message.BaseMessage;

public class MessageMetaInfo {

	/**
	 * 反射时操作处理微信消息的对象实例
	 */
	private Object obj;
	
	/**
	 * 反射时操作处理微信消息的对象实例的方法
	 */
	private Method method;
	
	private Class<? extends BaseMessage> messageBeanClass;

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}
	
	

	public Class<? extends BaseMessage> getMessageBeanClass() {
		return messageBeanClass;
	}

	public void setMessageBeanClass(Class<? extends BaseMessage> messageBeanClass) {
		this.messageBeanClass = messageBeanClass;
	}

	@Override
	public String toString() {
		return "MessageMetaInfo [obj=" + obj + ", method=" + method + ", messageBeanClass=" + messageBeanClass + "]";
	}

	
}
