package top.hmtools.wxmp.material.model;

public class MediaParam {

	private String media_id;

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	@Override
	public String toString() {
		return "MediaParam [media_id=" + media_id + "]";
	}
	
	
}
