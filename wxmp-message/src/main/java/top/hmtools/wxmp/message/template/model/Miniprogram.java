package top.hmtools.wxmp.message.template.model;

/**
 * Auto-generated: 2019-08-29 11:48:4
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Miniprogram {

	private String appid;
	private String pagepath;

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getAppid() {
		return appid;
	}

	public void setPagepath(String pagepath) {
		this.pagepath = pagepath;
	}

	public String getPagepath() {
		return pagepath;
	}

}