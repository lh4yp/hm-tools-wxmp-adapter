package top.hmtools.wxmp.message.group.model.tagGroupSend;

/**
 * Auto-generated: 2019-08-26 10:46:31
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class MediaId {

	private String media_id;

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	public String getMedia_id() {
		return media_id;
	}

}