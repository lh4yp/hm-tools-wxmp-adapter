package top.hmtools.wxmp.user.model.eventMessage;

import org.junit.Test;

import com.github.jsonzou.jmockdata.JMockData;

import top.hmtools.wxmp.core.DefaultWxmpMessageHandle;
import top.hmtools.wxmp.core.model.message.BaseMessage;
import top.hmtools.wxmp.user.enums.EUserEventMessages;

public class LocationMessageTest {

	@Test
	public void testToStr() {
		EUserEventMessages[] values = EUserEventMessages.values();
		for (EUserEventMessages item : values) {
			BaseMessage message = (BaseMessage) JMockData.mock(item.getClassName());
			System.out.println(message);
			System.out.println(message.toXmlMsg());
		}
	}

	@Test
	public void msgTest() {
		// 实例化消息处理handle
		DefaultWxmpMessageHandle defaultWxmpMessageHandle = new DefaultWxmpMessageHandle();

		// 实例化实际处理指定消息的controller，并加入handle映射
		UserMessageTestController menuMessageTestController = new UserMessageTestController();
		defaultWxmpMessageHandle.addMessageMetaInfo(menuMessageTestController);
		
		String xml = "<xml>    <ToUserName><![CDATA[toUser]]></ToUserName>    <FromUserName><![CDATA[fromUser]]></FromUserName>    <CreateTime>123456789</CreateTime>    <MsgType><![CDATA[event]]></MsgType>    <Event><![CDATA[LOCATION]]></Event>    <Latitude>23.137466</Latitude>    <Longitude>113.352425</Longitude>    <Precision>119.385040</Precision></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
	}

}
