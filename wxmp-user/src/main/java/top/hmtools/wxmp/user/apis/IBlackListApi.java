package top.hmtools.wxmp.user.apis;

import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.user.model.BatchBlackListParam;
import top.hmtools.wxmp.user.model.BlackListParam;
import top.hmtools.wxmp.user.model.BlackListResult;

@WxmpMapper
public interface IBlackListApi {

	/**
	 * 1. 获取公众号的黑名单列表
	 * @param blackListParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/tags/members/getblacklist")
	public BlackListResult getBlackList(BlackListParam blackListParam);
	
	/**
	 * 2. 拉黑用户
	 * @param batchBlackListParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/tags/members/batchblacklist")
	public ErrcodeBean batchBlackList(BatchBlackListParam batchBlackListParam);

	/**
	 * 3. 取消拉黑用户
	 * @param batchBlackListParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/tags/members/batchunblacklist")
	public ErrcodeBean batchUnblackList(BatchBlackListParam batchBlackListParam);
}
